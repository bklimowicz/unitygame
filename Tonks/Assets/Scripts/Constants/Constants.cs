﻿public static class Constants
{
    public static class Axis
    {
        public readonly static string Horizontal = "Horizontal";
        public readonly static string Vertical = "Vertical";
    }
}

