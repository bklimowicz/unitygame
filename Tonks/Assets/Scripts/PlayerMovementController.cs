﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    public float movementSpeed = 2.0f;
    public float rotationSpeed = 0.5f;

    private Rigidbody2D rigidbody;
    private Vector2 movementVector = new Vector2();
    // Start is called before the first frame update
    void Start()
    {
        rigidbody = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        float moveHorizontal = Input.GetAxis(Constants.Axis.Horizontal) * rotationSpeed;
        float moveVertical = Input.GetAxis(Constants.Axis.Vertical) * movementSpeed;
        movementVector = new Vector2(0, moveVertical);

        rigidbody.AddRelativeForce(Vector2.up * movementVector);
        rigidbody.rotation -= moveHorizontal;

        Debug.Log($"Horizontal: {moveHorizontal}. Vertical: {moveVertical}. Magnitude: {rigidbody.velocity.magnitude}");
    }
}
