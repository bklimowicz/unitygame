﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Tonks_Server
{
    public class Client
    {
        public static int DataBufferSize = 4096;

        public int Id;
        public TCP Tcp;

        public Client(int clientId)
        {
            this.Id = clientId;
            Tcp = new TCP(this.Id);
        }

        public class TCP
        {
            public TcpClient Socket;

            private readonly int Id;
            private NetworkStream Stream;
            private byte[] ReceiveBuffer;

            public TCP(int id)
            {
                this.Id = id;
            }

            public void Connect(TcpClient socket)
            {
                this.Socket = socket;
                socket.ReceiveBufferSize = DataBufferSize;
                socket.SendBufferSize = DataBufferSize;

                Stream = socket.GetStream();

                ReceiveBuffer = new byte[DataBufferSize];

                Stream.BeginRead(ReceiveBuffer, 0, DataBufferSize, ReceiveCallback, null);

                // TODO: send welcome packet
            }

            private void ReceiveCallback(IAsyncResult result)
            {
                try
                {
                    int byteLength = Stream.EndRead(result);
                    if (byteLength <= 0)
                    {
                        // TODO: Disconnect
                        return;
                    }

                    byte[] data = new byte[byteLength];
                    Array.Copy(ReceiveBuffer, data, byteLength);

                    // TODO: Handle data
                    Stream.BeginRead(ReceiveBuffer, 0, DataBufferSize, ReceiveCallback, null);

                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error receiving TCP data: {ex}");
                    // TODO: Disconnect
                }
            }
        }
    }
}
